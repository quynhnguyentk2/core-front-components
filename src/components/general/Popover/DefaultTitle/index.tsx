import React from 'react';

import resetIcon from '../../../../assets/svg/reset.svg';

export interface DefaultTitleProps {
  subTitle?: string;
  subAction?: string;

  handleResetSetting?: () => void;
}

const DefaultTitle: React.FC<DefaultTitleProps> = ({ subTitle, subAction, handleResetSetting }) => {
  return (
    <div className="popover__title__wrapper">
      <div className="popover__title">{subTitle ?? 'Grid Settings'}</div>
      <div className="popover__resetLink" role="button" onClick={handleResetSetting}>
        <img className="popover__resetLink__icon" src={resetIcon} alt="reset" />
        <span>{subAction ?? 'Reset'}</span>
      </div>
    </div>
  );
};

export default DefaultTitle;
