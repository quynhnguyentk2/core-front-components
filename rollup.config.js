import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import typescript from 'rollup-plugin-typescript2';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';

import postcss from 'rollup-plugin-postcss';
import prefixer from 'postcss-prefix-selector';
import copy from 'rollup-plugin-copy';
import svg from 'rollup-plugin-svg';

import pkg from './package.json';

export default {
  input: 'src/components/index.tsx',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: true,
      strict: false,
    },
    // {
    //   file: pkg.module,
    //   format: 'esm',
    //   sourcemap: true,
    // },
  ],

  plugins: [
    peerDepsExternal(),

    postcss({
      plugins: [
        prefixer({
          prefix: '.damsanx-core',
        }),
      ],
      extensions: ['.css', '.scss', '.less'],
      use: ['sass', ['less', { javascriptEnabled: true }]],
      extract: true,
    }),
    svg({ base64: true }),

    resolve({ preferBuiltins: false }),
    typescript({ objectHashIgnoreUnknownHack: true, clean: true }),

    copy({
      targets: [
        { src: 'src/assets/vendors/fonts/NoirPro-Bold.eot', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Bold.woff', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Bold.woff2', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Heavy.eot', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Heavy.woff', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Heavy.woff2', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Light.eot', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Light.woff', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Light.woff2', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Medium.eot', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Medium.woff', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Medium.woff2', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Regular.eot', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Regular.woff', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-Regular.woff2', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-SemiBold.eot', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-SemiBold.woff', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/NoirPro-SemiBold.woff2', dest: 'dist/fonts' },

        { src: 'src/assets/vendors/fonts/notification.eot', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/notification.svg', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/notification.ttf', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/notification.woff', dest: 'dist/fonts' },

        { src: 'node_modules/slick-carousel/slick/fonts/slick.eot', dest: 'dist/fonts' },
        { src: 'node_modules/slick-carousel/slick/fonts/slick.ttf', dest: 'dist/fonts' },
        { src: 'node_modules/slick-carousel/slick/fonts/slick.woff', dest: 'dist/fonts' },
        { src: 'node_modules/slick-carousel/slick/fonts/slick.svg', dest: 'dist/fonts' },

        { src: 'src/assets/vendors/fonts/gaxon.eot', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/gaxon.svg', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/gaxon.ttf', dest: 'dist/fonts' },
        { src: 'src/assets/vendors/fonts/gaxon.woff', dest: 'dist/fonts' },

        { src: 'node_modules/slick-carousel/slick/ajax-loader.gif', dest: 'dist' },
        { src: 'src/assets/vendors/flag/images/sprite-flags-24x24.png', dest: 'dist/images' },
      ],
    }),

    commonjs({
      include: 'node_modules/**',
    }),
  ],

  external: ['react', 'react-dom'],
};
