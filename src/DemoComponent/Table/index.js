import React from 'react';

import { Table, CardBox } from '../../components';

const data = [
  {
    name: 'name 1',
    standard: {
      name: 'standard 1',
      version: 'version 1',
      reportFormats: [3, 4, 5],
    },
    issueLists: [
      {
        name: 'issue 1',
      },
      {
        name: 'issue 2',
      },
    ],
    mappingVer: 'mappingVer 1',
    status: 'ACTIVE',
  },

  {
    name: 'name 2',
    standard: {
      name: 'standard 1',
      version: 'version 1',
      reportFormats: [3, 4, 5],
    },
    issueLists: [
      {
        name: 'issue 3',
      },
      {
        name: 'issue 4',
      },
    ],
    mappingVer: 'mappingVer 2',
    status: 'ACTIVE',
    createdAt: '05/30/1999',
  },
];

const columns = [
  {
    title: 'Equivalence Mapping',
    dataIndex: 'name',
    key: 'id',
    fixed: 'left',
    width: 300,
  },
  {
    title: 'Third Party Name',
    dataIndex: 'standard.name',
    key: 'standard.name',
    width: 200,
  },
  {
    title: 'Third Party Tool Version',
    dataIndex: 'standard.version',
    key: 'standard.version',
    width: 200,
    render: (record) => `Version ${record}`,
  },
  {
    title: 'Supported File',
    dataIndex: 'standard.reportFormats',
    key: 'standard.reportFormats',
    width: 200,
    render: (record) => {
      return record && record.length > 0 && record.map((item, index) => <span key={index}>{item}</span>);
    },
  },
  {
    title: 'Issue List',
    dataIndex: 'issueLists',
    key: 'issueLists',
    width: 200,
    render: (record) =>
      record && record.length > 0 && record.map((item, index) => <span key={index}>{item.name}</span>),
    defaultHidden: true,
  },
  {
    title: 'Mapping Version',
    dataIndex: 'mappingVer',
    key: 'mappingVer',
    width: 200,
    render: () => '1.0',
  },
  {
    title: 'Activate',
    dataIndex: 'status',
    key: 'status',
    width: 200,
  },
  {
    title: 'Created at',
    dataIndex: 'createdAt',
    key: 'createdAt',
    width: 200,
  },
  {
    title: 'Created by',
    dataIndex: 'createdBy',
    key: 'createdBy',
    width: 200,
  },
  {
    title: 'Updated at',
    dataIndex: 'updatedAt',
    key: 'updatedAt',
    width: 200,
  },
  {
    title: 'Updated by',
    dataIndex: 'updatedBy',
    key: 'updatedBy',
    width: 200,
  },
  {
    title: 'loz',
    key: 'action',
    width: 50,
    fixed: 'right',
    popoverConfig: true,
    // render: record => {
    //   const moreActionMenuItem = [
    //     {
    //       key: 'delete',
    //       name: 'Delete',
    //     },
    //     {
    //       key: 'edit',
    //       name: 'Edit',
    //     },
    //   ];
    //   const handleActionItemClick = () => {
    //     console.log('handleActionItemClick');
    //   };
    //   return (
    //     record.status === 'ACTIVE' && (
    //       <RsMoreActions
    //         menus={moreActionMenuItem}
    //         onMenuItemClick={() => handleActionItemClick(record)}
    //         placement="bottomRight"
    //       />
    //     )
    //   );
    // },
  },
];

const TableComponent = () => {
  return (
    <CardBox title="Basic">
      <Table dataSource={data} columns={columns} pagination={false} hasPopoverSetting />
    </CardBox>
  );
};

export default TableComponent;
