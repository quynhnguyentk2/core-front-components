import React from 'react';

import {
  // Button as ButtonComponent,
  Table as TableComponent,
} from './DemoComponent';

const App = () => (
  <div className="App">
    <TableComponent />
  </div>
);

export default App;
