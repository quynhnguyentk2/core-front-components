import React from 'react';
import _isEmpty from 'lodash/isEmpty';

import Button from 'antd/lib/button';
import Divider from 'antd/lib/divider';
import Checkbox from 'antd/lib/checkbox';

import { ColumnValue } from '../../../Table';

// Declare type
export interface PopoverContentProps {
  visible?: boolean;

  CancelButton?: React.ReactNode;
  ApplyButton?: React.ReactNode;

  columns: ColumnValue[];

  onClose?: () => void;
  onApply?: (columns: ColumnValue[]) => void;
  setColumns: (cols: any) => void;
}

const PopoverContent: React.FC<PopoverContentProps> = (props) => {
  const { onClose, onApply, columns, CancelButton, ApplyButton, setColumns } = props;

  const handleCancelChange = () => {
    // uncontrolled mode
    if (!onClose) return;

    onClose();
  };

  const handleSaveConfig = () => {
    if (!onApply) return;

    onApply(columns);
  };

  const DefaultCancelButton = (
    <Button type="default" onClick={handleCancelChange}>
      Cancel
    </Button>
  );

  const DefaultApplyButton = (
    <Button className="gx-ripple-effect" type="primary" onClick={handleSaveConfig}>
      Apply
    </Button>
  );

  const _CancelButton = CancelButton !== undefined ? CancelButton : DefaultCancelButton;
  const _ApplyButton = ApplyButton !== undefined ? ApplyButton : DefaultApplyButton;

  const handleColCheckBoxChange = (e: any) => {
    const { value } = e.target;
    const newCols = columns.map((item) => {
      if (item.title === value) {
        return !item.defaultHidden ? { ...item, defaultHidden: true } : { ...item, defaultHidden: false };
      }
      return item;
    });

    setColumns(newCols);
  };

  const renderList = (cols: ColumnValue[]) => {
    return (
      cols.length > 0 &&
      cols.map((_col) => {
        // eslint-disable-next-line array-callback-return
        if (_col.popoverConfig) return;

        return (
          <Checkbox
            key={_col.key}
            value={_col.title}
            onChange={handleColCheckBoxChange}
            defaultChecked={!_col.defaultHidden}
            disabled={!!_col.fixed}
          >
            {_col.title}
          </Checkbox>
        );
      })
    );
  };

  const renderColsSelectList = () => {
    const shownCols = columns ? columns.filter((item) => !item.defaultHidden) : [];
    const hiddenCols = columns ? columns.filter((item) => item.defaultHidden) : [];
    const notShowDivider = _isEmpty(hiddenCols) || _isEmpty(shownCols);

    return (
      <div className="popover__selectList">
        {renderList(shownCols)}
        {!notShowDivider && <Divider />}
        {renderList(hiddenCols)}
      </div>
    );
  };

  return (
    <div className="popover__content__wrapper">
      {renderColsSelectList()}
      <div className="popover__button">
        {_CancelButton}
        {_ApplyButton}
      </div>
    </div>
  );
};

export default PopoverContent;
