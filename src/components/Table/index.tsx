import React, { ReactNode, useState } from 'react';
import ClassNames from 'classnames';
import _sumBy from 'lodash/sumBy';
import { Table } from 'antd';

import Popover, { PopoverProps } from '../general/Popover';
import settingIcon from '../../assets/svg/setting.svg';

// Declared type

export declare type TableSize = 'small' | 'middle';
export declare type SortOrder = 'descend' | 'ascend' | null;
export declare type CompareFn<T> = (a: T, b: T, sortOrder?: SortOrder) => number;
export interface ColumnFilterItem {
  text: React.ReactNode;
  value: string | number | boolean;
  children?: ColumnFilterItem[];
}

// Defined interface

export interface ColumnValue {
  title: string;
  key: string;
  dataIndex?: string;
  popoverConfig?: boolean;

  render?: (text: any, record: any) => ReactNode;
  [key: string]: any;
}

export interface TableValue {
  key?: string | number;
  name?: React.ReactNode;
  [key: string]: any;
}

export interface TableProps {
  className?: string;
  loading?: boolean;
  bordered?: boolean;
  hasPopoverSetting?: boolean;

  dataSource: TableValue[];
  columns: ColumnValue[];
  popoverSetting?: PopoverProps;

  size?: TableSize;
  rowSelection?: {
    onChange?: (selectedRowKeys?: any, selectedRows?: any) => void;
    getCheckboxProps?: (record?: any) => Record<string, unknown>;
  };

  pagination?: false | any;
  scroll?: {
    x?: number | true;
    y?: number;
    scrollToFirstRowOnChange?: boolean;
  };

  title?: () => React.ReactNode;
  footer?: () => React.ReactNode;
  expandedRowRender?: (record: any) => React.ReactNode;
  onChange?: (pagination: any, filters: any, sorter: any, extra: any) => void;
}

const TableComponent: React.FC<TableProps> = (props) => {
  const {
    columns,
    dataSource,
    size,
    rowSelection,
    scroll,
    loading,
    pagination,
    bordered,
    className,
    popoverSetting,
    hasPopoverSetting,
    title,
    footer,
    expandedRowRender,
    onChange,
  } = props;

  const [_columns, setColumns] = useState<ColumnValue[]>(columns);
  const [open, setOpen] = useState(false);

  const handleShowGridSetting = () => {
    setOpen(!open);
  };

  const handleHideGridSetting = () => {
    setOpen(false);
  };

  const handleApplySetting = (cols: ColumnValue[]) => {
    setColumns(cols);
    setOpen(false);
  };

  const tempCols: any = _columns.map((col: ColumnValue) => {
    if (col.popoverConfig) {
      return {
        ...col,
        title: (
          <Popover
            visible={open}
            columns={_columns}
            trigger="click"
            placement="bottomRight"
            onClose={handleHideGridSetting}
            onApply={handleApplySetting}
            onVisibleChange={handleShowGridSetting}
            {...popoverSetting}
          >
            <div className="table__column__setting">
              <img src={settingIcon} alt="" />
            </div>
          </Popover>
        ),
      };
    }
    return col;
  });

  const shownCols: ColumnValue[] = tempCols.filter((item: ColumnValue) => !item.defaultHidden);

  const getScrollOffset = () => {
    if (hasPopoverSetting) {
      return {
        x: _sumBy(columns, 'width'),
      };
    }
    return scroll;
  };

  return (
    <Table
      {...props}
      size={size}
      loading={loading}
      bordered={bordered}
      columns={shownCols}
      dataSource={dataSource}
      pagination={pagination}
      rowSelection={rowSelection}
      title={title}
      footer={footer}
      onChange={onChange}
      scroll={getScrollOffset()}
      expandedRowRender={expandedRowRender}
      className={ClassNames('gx-table-responsive', className)}
    />
  );
};

export default TableComponent;
