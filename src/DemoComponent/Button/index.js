import React from 'react';

import { general, CardBox } from '../../components';

const { Button } = general;

const ButtonComponent = () => {
  return (
    <div>
      <CardBox title="Basic">
        <Button type="primary">Primary</Button>
        <Button>Default</Button>
        <Button type="dashed">Dashed</Button>
      </CardBox>

      <CardBox title="Primary">
        <Button type="primary" className="btn btn-info">
          Info
        </Button>
        <Button type="primary" className="gx-btn-success">
          success
        </Button>
        <Button type="primary" className="gx-btn-warning">
          warning
        </Button>
        <Button type="primary" className="gx-btn-danger">
          Danger
        </Button>
      </CardBox>

      {/* <CardBox title="Icon">
        <Button type="primary" shape="circle" icon={<Icon className="icon-search" />} />
        <Button type="primary" icon={<Icon className="icon-search" onClick={() => console.log('click')} />}>
          Search
        </Button>
        <Button shape="circle" icon={<Icon className="icon-search" />} />
        <Button icon={<Icon className="icon-search" />}>Search</Button>
        <br />
        <Button shape="circle" icon={<Icon className="icon-sent" />} />
        <Button icon={<Icon className="icon-search" />}>Search</Button>
        <Button type="dashed" shape="circle" icon={<Icon className="icon-search" />} />
        <Button type="dashed" icon={<Icon className="icon-mic" />}>
          Search
        </Button>
      </CardBox> */}

      <CardBox title="Loading">
        <Button type="primary" loading>
          Loading
        </Button>
        <Button type="primary" size="small" loading>
          Loading
        </Button>
        <br />
        <Button type="primary" loading={true}>
          Click me!
        </Button>
        <br />
        <Button shape="circle" loading />
        <Button type="primary" shape="circle" loading />
      </CardBox>
    </div>
  );
};

export default ButtonComponent;
