const { override, addLessLoader } = require('customize-cra');

module.exports = override(
  addLessLoader({
    // modifyVars: { "@primary-color": "#1DA57A" },
    lessOptions: {
      javascriptEnabled: true,
      // modifyVars: { '@primary-color': '#1DA57A' },
    },
  })
);
