import React, { CSSProperties } from 'react';
import ClassNames from 'classnames';

export interface CardBoxProps {
  className?: string;
  childrenStyle?: string;

  width?: string | number;
  height?: string | number;

  title?: React.ReactNode;
  children?: React.ReactNode;

  style?: CSSProperties;
}

const CardBox: React.FC<CardBoxProps> = (props) => {
  const { title, children, className, childrenStyle, style, width, height } = props;

  return (
    <div className={ClassNames('gx-card', className)} style={{ width, height, ...style }}>
      {title && (
        <div className="gx-card-head">
          <h3 className="gx-card-title">{title}</h3>
        </div>
      )}
      <div className={ClassNames('gx-card-body', childrenStyle)}>{children}</div>
    </div>
  );
};

export default CardBox;
