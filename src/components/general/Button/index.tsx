import React from 'react';

import { Button } from 'antd';
import { ButtonProps } from 'antd/lib/button';

// Declared type

declare const ButtonShapes: ['circle', 'round'];
declare const ButtonHTMLTypes: ['submit', 'button', 'reset'];
declare const ButtonTypes: ['default', 'primary', 'ghost', 'dashed', 'link', 'text'];

export declare type ButtonType = typeof ButtonTypes[number];
export declare type ButtonShape = typeof ButtonShapes[number];
export declare type ButtonHTMLType = typeof ButtonHTMLTypes[number];

export declare type LegacyButtonType = ButtonType | 'danger';
export declare type SizeType = 'small' | 'middle' | 'large' | undefined;

// Defined interface

export interface ButtonComponentProps extends ButtonProps {
  loading?:
    | boolean
    | {
        delay?: number;
      };
  size?: SizeType;

  ghost?: boolean;
  block?: boolean;
  danger?: boolean;
  disabled?: boolean;

  href?: string;
  target?: string;
  className?: string;

  width?: number | string;
  height?: number | string;

  icon?: React.ReactNode;
  children?: React.ReactNode;

  type?: ButtonType;
  shape?: ButtonShape;

  onClick?: React.MouseEventHandler<HTMLElement>;
}

const ButtonComponent: React.FC<ButtonComponentProps> = (props) => {
  const {
    target,
    className,
    href,
    icon,
    loading,
    size,
    ghost,
    disabled,
    type,
    block,
    width,
    height,
    children,
    onClick,
  } = props;

  return (
    <Button
      {...props}
      type={type}
      icon={icon}
      size={size}
      href={href}
      ghost={ghost}
      block={block}
      target={target}
      loading={loading}
      disabled={disabled}
      className={className}
      style={{ width, height }}
      onClick={onClick}
    >
      {children}
    </Button>
  );
};

export default ButtonComponent;
