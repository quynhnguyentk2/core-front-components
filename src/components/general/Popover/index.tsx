import React, { useState } from 'react';

import { Popover } from 'antd';
import { AbstractTooltipProps } from 'antd/lib/tooltip';

import { ColumnValue } from '../../Table';
import DefaultTitle from './DefaultTitle';
import DefaultContent from './DefaultContent';

export declare type ActionType = string;
export declare type PopoverPlacement =
  | 'top'
  | 'left'
  | 'right'
  | 'bottom'
  | 'topLeft'
  | 'topRight'
  | 'bottomLeft'
  | 'bottomRight'
  | 'leftTop'
  | 'leftBottom'
  | 'rightTop'
  | 'rightBottom';

export interface PopoverProps extends AbstractTooltipProps {
  subAction?: string;
  subTitle?: string;
  visible?: boolean;

  content?: React.ReactNode;
  title?: string;
  children?: React.ReactElement;

  columns: ColumnValue[];
  trigger?: ActionType | ActionType[];
  placement?: PopoverPlacement;

  onClose?: () => void;
  onApply?: (cols: any) => void;
}

const PopoverComponent: React.FC<PopoverProps> = (props) => {
  const { visible, content, title, children, trigger, columns, subAction, subTitle, ...rest } = props;
  const [_columns, setColumns] = useState<ColumnValue[]>(columns); // remove the last col

  const handleResetSetting = () => {
    setColumns(columns);
  };

  const _title =
    title !== undefined ? (
      title
    ) : (
      <DefaultTitle subTitle={subTitle} subAction={subAction} handleResetSetting={handleResetSetting} />
    );
  const __content =
    content !== undefined ? content : <DefaultContent {...props} columns={_columns} setColumns={setColumns} />;

  return (
    <div className="damsanx-core">
      <Popover
        overlayClassName="popover__container"
        visible={visible}
        content={__content}
        title={_title}
        trigger={trigger}
        {...rest}
      >
        {children}
      </Popover>
    </div>
  );
};

export default PopoverComponent;
